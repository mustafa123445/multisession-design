<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/assets/css/style.css">
    <title>Çoklu oturum</title>
</head>

<body>
    <h1>Kim izliyor?</h1>
    <br />
    <?php for ($x = 1; $x <= 2; $x++) { ?>
        <div class="user-box">
            <div class="header">
                <img src="https://i.pinimg.com/564x/0e/35/8d/0e358d98578648662715198235ce64ee.jpg" />
            </div>
            <p>
                Jason <?= $x ?>
            </p>
        </div>
    <?php } ?>

    <div class="user-box">
        <div class="header" style="border: none;">
            <b style="position: relative; top: 34%; left: 0px;">
                +
            </b>
        </div>
        <p>
            Add New
        </p>
    </div>

    <script href="/assets/js/script.js"></script>
</body>

</html>